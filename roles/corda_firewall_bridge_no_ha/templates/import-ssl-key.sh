{{ java_home }}/bin/java -jar \
    {{ bridge_configuration.directories.root }}/utils/corda-tools-ha-utilities-{{ corda_version }}.jar \
    import-ssl-key \
    --bridge-keystore={{ bridge_configuration.directories.root }}/certificates/nodeUnitedSslKeystore.jks \
    --node-keystores={{ bridge_configuration.directories.root }}/validating_node/certificates/sslkeystore.jks \
    --node-keystore-passwords={{ hostvars[groups.corda_node_no_ha[0]].node.key_store_password  }} \
    {% for sslkeystore in sslkeystore_files.stdout_lines %}
    --node-keystores={{ sslkeystore }} \
    --node-keystore-passwords={{ hostvars[groups.notary[0]].notary.key_store_password }} \
    {% endfor %}
    --bridge-keystore-password={{ bridge_keystore_password }}